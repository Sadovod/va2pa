/* -------------------------------------------------------------------------- */
/*                                 DEFINITIONS                                */
/* -------------------------------------------------------------------------- */


use std::mem::{size_of_val, size_of, MaybeUninit};
use std::any::Any;

use rand::Rng;

use crate::TranslationError::{PDENotPresent, PDEReserved, PDESupervisorMode, PDPTENotPresent, PDPTEReserved, PML4ENotPresent, PML4EReserved, PML4ESupervisorMode, PTENotPresent, PTEReserved, PTESetPAT, PTESupervisorMode, RamReadError};


const MAXPHYADDR: u8 = 48;


// Page map entry bits (PTE, PDE, PDPTE, PML4E)
// .
// #[derive(Default)]   can not use Default::default() for static instance,
// so we need to create default static instance
struct PMEBits {
    present: Option<u8>,
    writable: Option<u8>,
    user_access: Option<u8>,
    pwt: Option<u8>,
    pcd: Option<u8>,
    accessed: Option<u8>,
    dirty: Option<u8>,
    pse: Option<u8>,        // page size extension (2Mb for PDE, 1Gb for PDPTE)
    pat: Option<u8>,        // for PTE
    rsvd: Option<u8>,       // for PML4E and higher maps (as PML5)
    global: Option<u8>,
    addr_start: Option<u8>,
    addr_end: Option<u8>,
    reserved: Option<u64>    // reserved bits mask
} // BitTable

struct CR3Bits {
    pwt: u8,
    pcd: u8,
    addr_start: u8,
    addr_end: u8,
    reserved: u64
}


static CR3_BITS: CR3Bits = CR3Bits {
    pwt: 3,
    pcd: 4,
    addr_start: 12,
    addr_end: MAXPHYADDR - 1,
    reserved: 0xFFFF000000000000
};

static DEFAULT_PME_BITS: PMEBits = PMEBits {
    present: Some(0),
    writable: Some(1),
    user_access: Some(2),
    pwt: Some(3),
    pcd: Some(4),
    accessed: Some(5),
    dirty: None,
    pse: None,
    pat: None,
    rsvd: None,
    global: None,
    addr_start: None,
    addr_end: Some(MAXPHYADDR - 1),
    reserved: None
};

static PTE_BITS: PMEBits = PMEBits {
    dirty: Some(6),
    pat: Some(7),
    global: Some(8),
    addr_start: Some(12),
    reserved: Some(0xF000000000000),    // 48 - 52
    ..DEFAULT_PME_BITS
};

static PDE_BITS: PMEBits = PMEBits {
    pse: Some(7),   // must be unset
    addr_start: Some(12),
    reserved: Some(0xF000000000000),
    ..DEFAULT_PME_BITS
};

static PDE2MB_BITS: PMEBits = PMEBits { // 2Mb page (pse is set)
    pse: Some(7),   // must be set
    addr_start: Some(21),
    reserved: Some(0xF0000000FF000),
    ..DEFAULT_PME_BITS
};

static PDPTE_BITS: PMEBits = PMEBits {
    pse: Some(7),   // must be unset
    addr_start: Some(12),
    reserved: Some(0xF000000000000),
    ..DEFAULT_PME_BITS
};

static PDPTE1GB_BITS: PMEBits = PMEBits { // 1Gb page (pse is set)
    pse: Some(7),   // must be set
    addr_start: Some(30),
    reserved: Some(0xF00000FFFF000),
    ..DEFAULT_PME_BITS
};

static PML4E_BITS: PMEBits = PMEBits {
    rsvd: Some(7),  // must be unset
    addr_start: Some(12),
    reserved: Some(0xF000000000000),
    ..DEFAULT_PME_BITS
};


enum TranslationError {
    RamReadError,               // PREAD_FUNC returned 0 thus an error occurred
    PTENotPresent,              // Present bit of PTE is not set
    PDENotPresent,              // Present bit of PDE is not set
    PDPTENotPresent,            // Present bit of PDPTE is not set
    PML4ENotPresent,            // Present bit of PDE is not set
    PTESupervisorMode,          // PTE is in supervisor mode and cannot be accessed
    PDESupervisorMode,          // PDE is in supervisor mode and cannot be accessed
    PDPTESupervisorMode,        // PDPTE is in supervisor mode and cannot be accessed
    PML4ESupervisorMode,        // PDE is in supervisor mode and cannot be accessed
    PTEWith2MbPDE,              // PS bit is set therefore directory is inaccessible in 4Kb mode (only with PSE enabled)
    PDEWith1GbPDPTE,            // PS bit is set therefore directory is inaccessible in 2Mb mode (only with PSE enabled)
    PTEReserved,                // PTE reserved bits are set
    PDEReserved,                // PDE reserved bits are set
    PDPTEReserved,              // PDPTE reserved bits are set
    PML4EReserved,              // PML4E reserved bits are set
    PTESetPAT                   // PAT bit should be zero in PSE mode
}


/* -------------------------------------------------------------------------- */
/*                                AUX FUNCTIONS                               */
/* -------------------------------------------------------------------------- */


fn rand_virtual_addr() -> u64{
    let v_addr: u64 = rand::thread_rng().gen_range(0..u64::MAX);
    v_addr >> 16    // only 48 bits used for virtual address
}

fn rand_data_bits(bytes: usize) -> u64 {
    let mut data: u64 = 0;
    for _ in 0..bytes {
        data |= rand::thread_rng().gen_range(0..u8::MAX) as u64;
        data <<= 8;
    }
    data
}

// Test function needed for read data from page map entry
fn read_func_64(buf: *mut dyn Any, size: usize, phys_addr: u64) -> usize {
    let target_data: u64 = rand_data_bits(size) | 0x5; // setting present bit and user access bit
    unsafe {
        if let Some(buffer) = buf.as_mut().unwrap().downcast_mut::<u64>() {
            *buffer = target_data
        }
    }
    size_of_val(&target_data)
}


/* -------------------------------------------------------------------------- */
/*                           MAIN API IMPLEMENTATION                          */
/* -------------------------------------------------------------------------- */


type PReadFunc = fn(buf: *mut dyn Any, size: usize, phys_addr: u64) -> usize;


// Returning phys_addr or error
fn va2pa(v_addr: u64, cr3_bits: u64, pread_func: PReadFunc) -> Result<u64, TranslationError> {
    // PML4E
    let mut pml4e = MaybeUninit::<u64>::uninit().as_mut_ptr();
    let pml4e_addr: u64 = ((cr3_bits >> CR3_BITS.addr_start) & 0xFFFFFFFFF) + ((v_addr >> 39) & 0x1FF) * size_of::<u64>() as u64;

    if pread_func(pml4e, size_of::<u64>(), pml4e_addr) < size_of::<u64>() {
        return Err(RamReadError)
    }
    let pml4e_bits: u64 = unsafe { *(pml4e.as_ref().unwrap()) };

    if pml4e_bits & (1 << PML4E_BITS.present.unwrap()) == 0 {   // check if present
        return Err(PML4ENotPresent)
    } else if pml4e_bits & (1 << PML4E_BITS.user_access.unwrap()) == 0 {
        return Err(PML4ESupervisorMode)
    } else if pml4e_bits & PML4E_BITS.reserved.unwrap() == 0 {
        return Err(PML4EReserved)
    }

    // PDPTE
    let mut pdpte = MaybeUninit::<u64>::uninit().as_mut_ptr();
    let pdpte_addr = ((pml4e_bits >> PML4E_BITS.addr_start.unwrap()) & 0xFFFFFFFFF) + ((v_addr >> 30) & 0x1FF) * size_of::<u64>() as u64;

    if pread_func(pdpte, size_of::<u64>(), pdpte_addr) < size_of::<u64>() {
        return Err(RamReadError)
    }
    let pdpte_bits = unsafe { *(pdpte.as_ref().unwrap()) };

    if (pdpte_bits & (1 << PDPTE_BITS.present.unwrap())) == 0 {
        return Err(PDPTENotPresent)
    } else if pdpte_bits & (1 << PML4E_BITS.user_access.unwrap()) == 0 {
        return Err(PML4ESupervisorMode)
    }

    if (pdpte_bits & (1 << PDPTE_BITS.pse.unwrap())) != 0 { // IF 1Gb PDPTE PSE IS ENABLED
        if pdpte_bits & PDPTE1GB_BITS.reserved.unwrap() != 0 {
            return Err(PDPTEReserved)
        }
        let phy_addr: u64 = (pdpte_bits & 0xFFFFC0000000) + (v_addr & 0x3FFFFFFF); // perhaps there is a mistake here
        return Ok(phy_addr)     // End of function if PDPTE PSE enabled
    } else {
        if pdpte_bits & PDPTE_BITS.reserved.unwrap() != 0 {
            return Err(PDPTEReserved)
        }
    }

    // PDE
    let mut pde = MaybeUninit::<u64>::uninit().as_mut_ptr();
    let pde_addr = ((pdpte_bits >> PDPTE_BITS.addr_start.unwrap()) & 0xFFFFFFFFF) + ((v_addr >> 21) & 0x1FF) * size_of::<u64>() as u64;

    if pread_func(pde, size_of::<u64>(), pde_addr) < size_of::<u64>() {
        return Err(RamReadError)
    }
    let pde_bits = unsafe { *(pde.as_ref().unwrap()) };

    if pde_bits & (1 << PDE_BITS.present.unwrap()) == 0 {
        return Err(PDENotPresent)
    } else if pde_bits & (1 << PDE_BITS.user_access.unwrap()) == 0 {
        return Err(PDESupervisorMode)
    }

    if pde_bits & (1 << PDE_BITS.pse.unwrap()) != 0 {   // Check is PSE enabled
        if pde_bits & (1 << PDE2MB_BITS.reserved.unwrap()) != 0 {
            return Err(PDEReserved)
        }
        let phy_addr: u64 = (pde_bits & 0x3FFFE00000) + (v_addr & 0x1FFFFF); // perhaps there is a mistake here
        return Ok(phy_addr)     // End of function if PDE PSE enabled
    } else {
        if pde_bits & (1 << PDE_BITS.reserved.unwrap()) != 0 {
            return Err(PDEReserved)
        }
    }

    // PTE
    let mut pte = MaybeUninit::<u64>::uninit().as_mut_ptr();
    let pte_addr = ((pde_addr >> PDE_BITS.addr_start.unwrap()) & 0xFFFFFFFFF) + ((v_addr >> 12) & 0x1FF) * size_of::<u64>() as u64;

    if pread_func(pte, size_of::<u64>(), pte_addr) < size_of::<u64>() {
        return Err(RamReadError)
    }
    let pte_bits = unsafe { *(pte.as_ref().unwrap()) };

    if pte_bits & (1 << PTE_BITS.present.unwrap()) == 0 {   // Check PTE present bit
        return Err(PTENotPresent)
    } else if pte_bits & (1 << PTE_BITS.user_access.unwrap()) == 0 {
        return Err(PTESupervisorMode)
    } else if pte_bits & (1 << PTE_BITS.pat.unwrap()) == 0 {
        return Err(PTESetPAT)
    } else if pte_bits & (1 << PTE_BITS.reserved.unwrap()) != 0 {
        return Err(PTEReserved)
    }

    let phy_addr = (pte_bits & 0xFFFFFFFFF000) + (v_addr & 0xFFF);
    Ok(phy_addr)
}


fn main() {
    let pread_func: PReadFunc = read_func_64;
    let result = va2pa(
        rand_virtual_addr(), rand_data_bits(8), pread_func
    );
    match result {
        Ok(phy_addr) => println!("Success, physical address = {}", phy_addr),
        Err(error) => println!("Failed, error code: {}", error as u64)
    }
}
